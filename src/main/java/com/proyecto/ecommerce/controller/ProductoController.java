package com.proyecto.ecommerce.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.swing.JOptionPane;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.proyecto.ecommerce.model.Producto;
import com.proyecto.ecommerce.model.Usuario;
import com.proyecto.ecommerce.service.ProductoService;
import com.proyecto.ecommerce.service.UploadFileService;







@Controller
@RequestMapping("/productos")
public class ProductoController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(ProductoController.class); 
	
	@Autowired
	private ProductoService productoService; 
	
	@Autowired
	private UploadFileService upload; 
	
	@GetMapping("")
	public String show(Model model) {
		LOGGER.info("Llamando al métpdo show del controlador.");
		List<Producto> productos = productoService.findAll(); 
		LOGGER.info("Numero de productos recuperados: {}", productos.size());
		model.addAttribute("productos", productos); 
		return "productos/show";
	}
	
	
	@GetMapping("/create")
	public String create() {
		return "productos/create";
	}
	
	@PostMapping("/save")
	public String save(Producto producto, @RequestParam("img")  MultipartFile file) throws IOException {
		LOGGER.info("Este es el obj producto {}", producto);
		Usuario u = new Usuario(1, "", "", "", "", "", "", "");
		producto.setUsuario(u);
		
		
		//imagen
		String nombreImagen = upload.saveImage(file); 
		producto.setImagen(nombreImagen); 
		
		productoService.save(producto); 
		return "redirect:/productos"; 
	}
	
	
	@GetMapping("/edit/{producto_id}")
	public String edit(@PathVariable Integer producto_id, Model model) {
		Producto producto = new Producto(); 
		Optional<Producto> optionalProducto = productoService.get(producto_id); 
		producto = optionalProducto.get(); 
		
		LOGGER.info("Producto buscado: {}", producto); 
		model.addAttribute("producto", producto); 
		return "productos/edit"; 
	}
	
	
	@PostMapping("/update")
	public String update(Producto producto, @RequestParam("img")  MultipartFile file) throws IOException {
		
		Producto  p = new Producto(); 
		p = productoService.get(producto.getProductoId()).get(); 
		
		
		
		if (file.isEmpty()) {//cuando no cambiamos imagen
			
			 
			producto.setImagen(p.getImagen()); 
		}else {//cuando cambiamos imagen
			
			if (!p.getImagen().equals("default.jpg")) {
				upload.deleteImage(p.getImagen()); 
			}
			
			String nombreImagen = upload.saveImage(file); 
			producto.setImagen(nombreImagen); 
		}
		
		producto.setUsuario(p.getUsuario()); 
		productoService.update(producto); 
		return "redirect:/productos";
	}
	
	@GetMapping("/delete/{producto_id}")
	public String delete(@PathVariable Integer producto_id) {
		
		Producto p = new Producto(); 
		p=productoService.get(producto_id).get(); 
		
		if (p.getImagen() !="default.jpg") {
			upload.deleteImage(p.getImagen()); 
		}
		
		productoService.delete(producto_id); 
		return "redirect:/productos";
	}
}
