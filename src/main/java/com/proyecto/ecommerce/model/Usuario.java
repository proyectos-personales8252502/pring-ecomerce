package com.proyecto.ecommerce.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "usuario_id")
	private Integer usuarioIdInteger; 
	private String nombre; 
	@Column(name = "user_name")
	private String userName; 
	private String email; 
	private String direccion; 
	private String tefelono; 
	private String tipo; 
	private String clave;
	
	
	@OneToMany(mappedBy = "usuario")
	private List<Producto> productos; 
	
	@OneToMany(mappedBy = "usuario")
	private List<Orden> ordenes; 
	
	public Usuario() {
		
	}
	
	public Usuario(Integer usuarioIdInteger, String nombre, String userName, String email, String direccion,
			String tefelono, String tipo, String clave) {
		super();
		this.usuarioIdInteger = usuarioIdInteger;
		this.nombre = nombre;
		this.userName = userName;
		this.email = email;
		this.direccion = direccion;
		this.tefelono = tefelono;
		this.tipo = tipo;
		this.clave = clave;
	}
	public Integer getUsuarioIdInteger() {
		return usuarioIdInteger;
	}
	public void setUsuarioIdInteger(Integer usuarioIdInteger) {
		this.usuarioIdInteger = usuarioIdInteger;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTefelono() {
		return tefelono;
	}
	public void setTefelono(String tefelono) {
		this.tefelono = tefelono;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Usuario [usuarioIdInteger=");
		builder.append(usuarioIdInteger);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", userName=");
		builder.append(userName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", direccion=");
		builder.append(direccion);
		builder.append(", tefelono=");
		builder.append(tefelono);
		builder.append(", tipo=");
		builder.append(tipo);
		builder.append(", clave=");
		builder.append(clave);
		builder.append("]");
		return builder.toString();
	} 
	
	
}
