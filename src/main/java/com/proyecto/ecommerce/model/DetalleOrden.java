package com.proyecto.ecommerce.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "detalle_orden")
public class DetalleOrden {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orden_detalle_id")
	private Integer detalleOrdenId; 
	private String nombre; 
	private double cantidad; 
	private double precio;
	private double total; 
	
	@OneToOne
	private Orden orden; 
	
	@ManyToOne
	private Producto producto; 
	
	public DetalleOrden() {
		// TODO Auto-generated constructor stub
	}

	

	public DetalleOrden(Integer detalleOrdenId, String nombre, double cantidad, double precio, double total, Orden orden,
			Producto producto) {
		super();
		this.detalleOrdenId = detalleOrdenId;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.precio = precio;
		this.total = total;
		this.orden = orden;
		this.producto = producto;
	}



	public Integer getDetalleOrdenId() {
		return detalleOrdenId;
	}

	public void setDetalleOrdenId(Integer detalleOrdenId) {
		this.detalleOrdenId = detalleOrdenId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	

	public Orden getOrden() {
		return orden;
	}

	public void setOdren(Orden odren) {
		this.orden = orden;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DetalleOrden [detalleOrdenId=");
		builder.append(detalleOrdenId);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", precio=");
		builder.append(precio);
		builder.append(", total=");
		builder.append(total);
		builder.append("]");
		return builder.toString();
	}
	
	
}
