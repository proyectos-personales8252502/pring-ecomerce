package com.proyecto.ecommerce.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "orden")
public class Orden {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "orden_id")
	private Integer id; 
	private String numero; 
	@Column(name = "fecha_creacion")
	private Date fechaCreacion; 
	@Column(name = "fecha_recibida")
	private Date fechaRecibida; 
	private double total; 
	
	@ManyToOne
	private Usuario usuario; 
	
	@OneToOne(mappedBy = "orden")
	private DetalleOrden detalleOrden; 
	
	public Orden() {
		// TODO Auto-generated constructor stub
	}

	

	



	public Orden(Integer id, String numero, Date fechaCreacion, Date fechaRecibida, double total, Usuario usuario,
			DetalleOrden detalleOrden) {
		super();
		this.id = id;
		this.numero = numero;
		this.fechaCreacion = fechaCreacion;
		this.fechaRecibida = fechaRecibida;
		this.total = total;
		this.usuario = usuario;
		this.detalleOrden = detalleOrden;
	}







	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaRecibida() {
		return fechaRecibida;
	}

	public void setFechaRecibida(Date fechaRecibida) {
		this.fechaRecibida = fechaRecibida;
	}

	
	
	public DetalleOrden getDetalleOrden() {
		return detalleOrden;
	}



	public void setDetalleOrden(DetalleOrden detalleOrden) {
		this.detalleOrden = detalleOrden;
	}



	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Orden [id=");
		builder.append(id);
		builder.append(", numero=");
		builder.append(numero);
		builder.append(", fechaCreacion=");
		builder.append(fechaCreacion);
		builder.append(", fechaRecibida=");
		builder.append(fechaRecibida);
		builder.append("]");
		return builder.toString();
	}
	
	
}
